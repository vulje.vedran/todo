﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using TODOWebApi.Dto;
using TODOWebApi.Models;

namespace TODOWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private List<User> users;

        public AuthController(IConfiguration configuration)
        {
            _configuration = configuration;

            var jsonData = System.IO.File.ReadAllText(Path.Combine("Data", "usersData.json"));
            var data = JsonConvert.DeserializeObject<List<User>>(jsonData);

            if (data is not null)
            {
                users = data;
            }
        }

        //[HttpPost("register")]
        //public ActionResult<User> Register(UserDto request)
        //{
        //    string passwordHash = BCrypt.Net.BCrypt.HashPassword(request.Pasword);

        //    user.Username = request.Username;
        //    user.PasswordHash = passwordHash;

        //    return Ok(user);
        //}

        [HttpPost("login")]
        public ActionResult<User> Login(UserDto request)
        {
            var user = users.FirstOrDefault(x => x.Username == request.Username && x.Password == request.Pasword);

            if (user == null)
                return BadRequest("Krivi Username ili lozinka");
            else
            {
                string token = CreateToken(user);

                return Ok(token);
            }

        }

        private string CreateToken(User user)
        {
            List<Claim> claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Role, user.Role)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                _configuration.GetSection("AppSettings:Token").Value!));

            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credentials
                );

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }
    }
}
