﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TODOWebApi.Dto;
using TODOWebApi.Models;

namespace TODOWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ToDoController : ControllerBase
    {
        private List<ToDo> todoItems;
        private static List<ToDo> addedItems = new List<ToDo>();
        private readonly string dataFilePath = Path.Combine("Data", "itemsData.json");

        public ToDoController()
        {
            if (System.IO.File.Exists(dataFilePath))
            {
                var jsonData = System.IO.File.ReadAllText(dataFilePath);
                var data = JsonConvert.DeserializeObject<List<ToDo>>(jsonData);

                if (data is not null)
                {
                    todoItems = data;
                }
            }
        }

        [HttpGet("getitems")]
        [Authorize(Roles = "Admin,User")]
        public IActionResult GetItems()
        {
            List<ToDo> allItems = new List<ToDo>(todoItems);
            allItems.AddRange(addedItems);
            return Ok(allItems);
        }

        [HttpPost("additem")]
        [Authorize(Roles = "Admin")]
        public IActionResult AddItem(ToDoDto item)
        {
            List<ToDo> allItems = new List<ToDo>(todoItems);

            var toDo = new ToDo()
            {
                Id = allItems.Any(x => x.Id > 0) ? allItems.Max(x => x.Id) + 1 : 1,
                Name = item.Name
            };

            addedItems.Add(toDo);
            allItems.AddRange(addedItems);

            return Ok(allItems);
        }
    }
}
